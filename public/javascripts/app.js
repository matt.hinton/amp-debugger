function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var aid = readCookie('aid');
var environment = readCookie('environment');

//These will be pulled in on subscribe page

//Enter offerId of any offer you'd like to show. 
const offerId = "OF7I1XB0JFF2";

//Enter templateId of template you'd like to use to display offer
const templateId = "OTZOFB78V4F3";

//Enter termId to see how a 1-term offer works
const termId = "TM9OKEZ17NE9";



//This is Piano's load scripts running. You don't need to do anything here
(function(src){var a=document.createElement("script");a.type="text/javascript";a.async=true;a.src=src;var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})(`https://${environment}.tinypass.com/xbuilder/experience/load?aid=${aid}`)
var createError = require('http-errors');
// const { aid, environment } = require('./public/amp-access-config.js');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
const pug = require('pug');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const cors = require('cors');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get('/news', function (req, res) {
  res.render('news.jade', {
    'aid': req.cookies.aid,
    'environment': req.cookies.environment
  });
})

app.get('/moreNews', function (req, res) {
  res.render('moreNews.jade', {
    'aid': req.cookies.aid,
    'environment': req.cookies.environment
  });
})

app.get('/premiumNews', function (req, res) {
  res.render('premiumNews.jade', {
    'aid': req.cookies.aid,
    'environment': req.cookies.environment
  });
})





// app.post('/ampConfig', jsonParser, submitFormXHRInputText);

// function submitFormXHRInputText(request, response) {
//   const email = request.body ? request.body.email : '';
//   const name = request.body ? request.body.name : '';
//   if (isUserTryingTheInputTextErrorDemo(name)) {
//     response.status(400);
//   }
//   response.json({
//     email,
//     name,
//   });
// }

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
});

module.exports = app;
//build application from Dockerfile
docker build .

//docker images to get image ID
docker run -p 3000:3000 -it {your image ID} npm start

//example
docker run -p 3000:3000 -it 5cc50cb91737 npm start


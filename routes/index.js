var express = require('express');
var router = express.Router();
var cookieParser = require('cookie-parser');

// a middleware function with no mount path. This code is executed for every request to the router
router.use(function (req, res, next) {
  console.log('Time:', Date.now())
  next()
})

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('entryPage', { title: 'Express' });
});


module.exports = router;
